package main

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type BlockMap map[*xir.Node]*Block

type Block struct {
	Switches   []string
	Nodes      []string
	Upstream   []int
	Downstream []int
}

func spineBlocks(topo *xir.Net) []*Block {

	var bs []*Block

	// get all the experiment spine switches from the topology
	spines := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Spine)
	})

	// map for tracking what nodes belong to what block
	bm := make(BlockMap)

	for _, x := range spines {

		nbrs := selectNeighbors(x, tb.Node, tb.NetworkEmulator)

		b, new := bm.Block(nbrs, nil)
		b.Switches = append(b.Switches, x.Label())

		if new {
			bs = append(bs, b)
		}

	}

	return bs

}

func fabricBlocks(topo *xir.Net) []*Block {

	var bs []*Block

	// get all the experiment fabric switches from the topology
	fabrics := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Fabric)
	})

	// map for tracking what nodes belong to what block
	bm := make(BlockMap)

	for _, x := range fabrics {

		nbrs := selectNeighbors(x, tb.Node, tb.NetworkEmulator)

		b, new := bm.Block(nbrs, nil)
		b.Switches = append(b.Switches, x.Label())

		if new {
			bs = append(bs, b)
		}

	}

	return bs

}

func leafBlocks(topo *xir.Net) []*Block {

	var result []*Block

	// map for tracking what nodes belong to what block
	bm := make(BlockMap)

	// get all the experiment leaf switches from the topology
	leaves := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Leaf)
	})

	for _, leaf := range leaves {

		// collect the nodes attached to each leaf
		nodes := selectNeighbors(leaf, tb.Node, tb.NetworkEmulator)

		// get a block for the nodes and add the leaf to that block
		b, new := bm.Block(nodes, nil)
		b.Switches = append(b.Switches, leaf.Label())

		if new {
			result = append(result, b)
		}

	}

	return result

}

func NewBlock() *Block {
	return &Block{
		Switches:   make([]string, 0),
		Nodes:      make([]string, 0),
		Upstream:   make([]int, 0),
		Downstream: make([]int, 0),
	}
}

func (b *Block) AddUpstream(index int) {
	for _, x := range b.Upstream {
		if x == index {
			return
		}
	}
	b.Upstream = append(b.Upstream, index)
}

func (b *Block) AddDownstream(index int) {
	for _, x := range b.Downstream {
		if x == index {
			return
		}
	}
	b.Downstream = append(b.Downstream, index)
}

func (bm BlockMap) Block(nodes []*xir.Node, blocks []*Block) (*Block, bool) {

	// try to find an existing block for any node in the set, if found use that
	// block
	var b *Block
	var isnew bool
	for _, x := range nodes {
		var ok bool
		b, ok = bm[x]
		if ok {
			break
		}
	}

	// if no block was found, create a new one and assign this set of nodes
	if b == nil {
		b = NewBlock()
		for _, x := range nodes {
			b.Nodes = append(b.Nodes, x.Label())
		}
		isnew = true
	} else {
		// ensure that any unassigned nodes are added to the block
		for _, x := range nodes {
			_, ok := bm[x]
			if !ok {
				b.Nodes = append(b.Nodes, x.Label())
			}
		}
	}

	// update the blockmap
	for _, x := range nodes {
		bm[x] = b
	}

	return b, isnew

}

func selectNeighbors(x *xir.Node, role ...tb.Role) []*xir.Node {

	var result []*xir.Node

	for _, n := range x.Neighbors() {
		if !tb.HasRole(n.Parent, role...) {
			continue
		}
		result = append(result, n.Parent)
	}

	return result

}

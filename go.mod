module gitlab.com/mergetb/tech/tbr

go 1.14

require (
	gitlab.com/mergetb/facilities/dcomp/model v0.1.3
	gitlab.com/mergetb/facilities/example v0.0.0-00010101000000-000000000000
	gitlab.com/mergetb/facilities/redstar/model v0.0.0-20200622234915-0a8ea7fb0426
	gitlab.com/mergetb/xir v0.2.17
	gitlab.com/praxistb/ops v0.0.0-00010101000000-000000000000
)

replace gitlab.com/mergetb/xir => /home/ry/src/gitlab.com/mergetb/xir

replace gitlab.com/mergetb/facilities/example => /home/ry/src/gitlab.com/mergetb/facilities/example

replace gitlab.com/mergetb/facilities/dcomp/model => /home/ry/src/gitlab.com/mergetb/facilities/dcomp/model

replace gitlab.com/praxistb/ops => /home/ry/src/gitlab.com/praxistb/ops

replace gitlab.com/mergetb/facilities/redstar/model => /home/ry/src/gitlab.com/mergetb/facilities/redstar/model

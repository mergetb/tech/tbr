const nodeSpacing = 10,
      tierSpacing = 75,
      step = 0.001,
      iters = 10000,
      ifxSize = 20,
      ifxSpacing = 5;

export default function Render(data) {

    var canvas = document.getElementById('tbview');
    var ctx = canvas.getContext('2d');


    var objs = bound(data);
    console.log(objs);

    ctx.fillStyle = '#cccccc77';
    ctx.strokeStyle = '#333';

    layout(ctx, objs);
    console.log(objs);

    ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight);
    renderMachines(ctx, objs.Machine)
    renderSwitches(ctx, objs.Leaf)

    renderSwitches(ctx, objs.Fabric)
    renderSwitches(ctx, objs.Spine)

}

function renderSwitches(ctx, switches) {

    for (var k in switches) {
        renderSwitch(ctx, switches[k]);
    }

}

function renderSwitch(ctx, sw) {

    ctx.fillRect(sw.Cloc.X, sw.Cloc.Y, width(sw), height(sw));
    ctx.strokeRect(sw.Cloc.X, sw.Cloc.Y, width(sw), height(sw));

}

function renderMachines(ctx, machines) {

    for(var k in machines) {
        renderMachine(ctx, machines[k]);
    }

}

function renderMachine(ctx, m) {

    ctx.fillRect(m.Cloc.X, m.Cloc.Y, width(m), height(m));
    ctx.strokeRect(m.Cloc.X, m.Cloc.Y, width(m), height(m));

    for(var i=0; i<m.Node.Interfaces.length; i++) {
        
        let x = m.Cloc.X + ifxSpacing + i*(ifxSize + ifxSpacing);
        let y = m.Cloc.Y + ifxSpacing;

        let fs = ctx.fillStyle;
        ctx.fillStyle = '#b2d6ef';
        ctx.fillRect(x, y, ifxSize, ifxSize);
        ctx.strokeRect(x, y, ifxSize, ifxSize);
        ctx.fillStyle = fs;
    }

}

function d2c(ctx, obj) {

    const h = ctx.canvas.clientHeight,
          w = ctx.canvas.clientWidth;

    return {X: obj.Loc.X, Y: h - height(obj) - obj.Loc.Y};

}

function bound(data) {

    var result = {
        Machine: {},
        Leaf: {},
        Fabric: {},
        Spine: {}
    };

    for(var key of data.MachineOrder) {
        var value = data.Machine[key]
        result.Machine[key] = newObj(value, nodeWidth(value), 50);
    }

    for(var key in data.Leaf) {
        var value = data.Leaf[key]
        result.Leaf[key] = newObj(value, 100, 50);
    }

    for(var key in data.Fabric) {
        var value = data.Fabric[key]
        result.Fabric[key] = newObj(value, 100, 50);
    }

    for(var key in data.Spine) {
        var value = data.Spine[key]
        result.Spine[key] = newObj(value, 100, 50);
    }

    return result;

}

function nodeWidth(n) {

    let N = n.Interfaces.length

    if(N <= 4) {
        return ifxSpacing + N*(ifxSize + ifxSpacing)
    }


    //TODO other cases

    return ifxSpacing + N*(ifxSpacing + ifxSize) + ifxSpacing
}

function newObj(value, w, h) {

    return {
        Node: value,
        Bounds: {
            Min: { X: 0, Y: 0 },
            Max: { X: w, Y: h },
        },
        Loc: {
            X: 0,
            Y: 0,
        },
        Cloc: {
            X: 0,
            Y: 0,
        },
        DX: 0,
    };

}


function layout(ctx, objs) {

    var i = 0;

    for(var key in objs.Machine) {
        var value = objs.Machine[key]
        value.Loc.X = i;
        value.Cloc = d2c(ctx, value)
        i += width(value) + nodeSpacing;
    }

    i = 0;
    for (var key in objs.Leaf) {
        var value = objs.Leaf[key];
        value.Loc.X = i;
        value.Loc.Y = tierSpacing;
        value.Cloc = d2c(ctx, value);
        i += width(value) + nodeSpacing;
    }
    alignLeaves(ctx, objs)
    for (var key in objs.Leaf) {
        var value = objs.Leaf[key];
        value.Cloc = d2c(ctx, value);
    }


    i = 0;
    for (var key in objs.Fabric) {
        var value = objs.Fabric[key];
        value.Loc.X = i;
        value.Loc.Y = 2*tierSpacing;
        value.Cloc = d2c(ctx, value)
        i += width(value) + nodeSpacing;
    }
    alignFabrics(ctx, objs)
    for (var key in objs.Fabric) {
        var value = objs.Fabric[key];
        value.Cloc = d2c(ctx, value);
    }

    i = 0;
    for (var key in objs.Spine) {
        var value = objs.Spine[key];
        value.Loc.X = i;
        value.Loc.Y = 3*tierSpacing;
        value.Cloc = d2c(ctx, value)
        i += width(value) + nodeSpacing;
    }
    alignSpines(ctx, objs)
    for (var key in objs.Spine) {
        var value = objs.Spine[key];
        value.Cloc = d2c(ctx, value);
    }

    console.log(objs)

}

function rightOverlap(a, b) {

    const aRightEdge = a.Loc.X + width(a) + nodeSpacing,
          bLeftEdge  = b.Loc.X;

    return aRightEdge - bLeftEdge;

}

function leftOverlap(a, b) {

    const aLeftEdge  = a.Loc.X,
          bRightEdge = b.Loc.X + width(b) + nodeSpacing;

    return bRightEdge - aLeftEdge;

}

function alignSpines(ctx, objs) {

    var spines = [];
    for (var s in objs.Spine) {
        spines.push(objs.Spine[s])
    }

    for(var i=0; i<iters; i++) {

        for(var k in objs.Fabric) {

            var f = objs.Fabric[k];
            for(var nbr of f.Node.Neighbors) {
                var s = objs.Spine[nbr.Remote.Node];
                if(s == null) continue;
                s.DX += (center(f).X - center(s).X)*step;
            }

        }


        for (var j=0; j<spines.length; j++) {

            var s = spines[j];
            if(s.DX > 0 && j<spines.length-1) {

                var overlap = rightOverlap(s, spines[j+1]);
                if(overlap > 0) {
                    s.DX = s.DX - overlap;
                    spines[j+1].DX += overlap;
                }

            }

            if(s.DX < 0 && j > 0) {
            
                var overlap = leftOverlap(s, spines[j-1])
                if(overlap > 0) {
                    s.DX = s.DX + overlap;
                    spines[j-1].DX -= overlap;
                }
            }

        }

        for (var j=0; j<spines.length; j++) {

            var s = spines[j];
            s.Loc.X += s.DX;
            s.DX = 0;

        }
    }

}

function alignFabrics(ctx, objs) {

    var fabrics = [];
    for (var f in objs.Fabric) {
        fabrics.push(objs.Fabric[f])
    }

    for(var i=0; i<iters; i++) {

        for(var k in objs.Leaf) {

            var l = objs.Leaf[k];
            for(var nbr of l.Node.Neighbors) {
                var f = objs.Fabric[nbr.Remote.Node];
                if(f == null) continue;
                f.DX += (center(l).X - center(f).X)*step;
            }

        }


        for (var j=0; j<fabrics.length; j++) {

            var f = fabrics[j];
            if(f.DX > 0 && j<fabrics.length-1) {

                var overlap = rightOverlap(f, fabrics[j+1]);
                if(overlap > 0) {
                    f.DX = f.DX - overlap;
                    fabrics[j+1].DX += overlap;
                }

            }

            if(f.DX < 0 && j > 0) {
            
                var overlap = leftOverlap(f, fabrics[j-1])
                if(overlap > 0) {
                    f.DX = f.DX + overlap;
                    fabrics[j-1].DX -= overlap;
                }
            }

        }

        for (var j=0; j<fabrics.length; j++) {

            var f = fabrics[j];
            f.Loc.X += f.DX;
            f.DX = 0;

        }
    }

}

function alignLeaves(ctx, objs) {
    
    var leaves = [];
    for(var l in objs.Leaf) {
        leaves.push(objs.Leaf[l])
    }

    for(var i=0; i<iters; i++) {

        for(var k in objs.Machine) {

            var m = objs.Machine[k];
            for(var nbr of m.Node.Neighbors) {
                var f = objs.Leaf[nbr.Remote.Node];
                f.DX += (center(m).X - center(f).X)*step;
            }

        }

        for (var j=0; j<leaves.length; j++) {

            var l = leaves[j];

            if(l.DX > 0 && j<leaves.length-1) {

                var overlap = rightOverlap(l, leaves[j+1]);
        
                if(overlap > 0) {
                    l.DX = l.DX - overlap;
                    leaves[j+1].DX += overlap;
                }


            }
            if(l.DX < 0 && j > 0) {

                var overlap = leftOverlap(l, leaves[j-1])

                if(overlap > 0) {
                    l.DX = l.DX + overlap
                    leaves[j-1].DX -= overlap;
                }

            }

        }

        for (var j=0; j<leaves.length; j++) {

            var l = leaves[j];

            l.Loc.X += l.DX;
            l.DX = 0;

        }

    }


}

function width(obj) {
    return obj.Bounds.Max.X - obj.Bounds.Min.X
}

function height(obj) {
    return obj.Bounds.Max.Y - obj.Bounds.Min.Y
}

function center(obj) {
    return { X: obj.Loc.X + width(obj)/2, Y: obj.Loc.Y + height(obj)/2 };
}

function xdist(a, b) {
    return center(a).X - center(b).X
}


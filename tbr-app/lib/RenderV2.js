// TODO
// - look into using https://github.com/fabricjs/fabric.js

const nodeFill = '#cccccc',
      nodeStroke = '#333',
      nodeSpacing = 10,
      nodeTextOffset = 2,
      ifxFill = '#aaa',
      ifxStroke = '#333',
      ifxSpacing = 5,
      ifxSize = 20,
      tierSpacing = 75,
      blockStroke = '#777',
      blockSpacing = 10,
      switchFill = '#b2d6ef',
      iters = 1000,
      step = 0.01;

var zoom = 1;
var data = {};
var dragging = false;
var drag = {x: 0, y: 0};
var pan =  {x: 0, y: 0};
var layout = {};
var ctx = {};
var canvas = {};
var bounds = {w: 0, h: 0};

export default function Render(_data) {

    data = _data;

    canvas = document.getElementById('tbview');
    ctx = canvas.getContext('2d');

    ctx.fillStyle = nodeFill
    ctx.strokeStyle = nodeStroke;

    layout = newLayout(ctx, data);
    render();

}

export function RenderZoom(e) {

    let w = ctx.canvas.clientWidth,
        h = ctx.canvas.clientHeight;

    let factor = e.deltaY * -0.001;
    zoom += factor;

    //TODO zoom in on mouse location with x/y translation
    let s = viewSize();
    ctx.clearRect(0, 0, s[0], s[1]);
    let t = ctx.getTransform();
    t.a = zoom;
    t.d = zoom;
    pan.x -= factor*bounds.w/2
    pan.y -= factor*bounds.h/2
    t.e = pan.x;
    t.f = pan.y;
    ctx.setTransform(t);

    render();


}


export function RenderMouseDown(e) {

    dragging = true;
    drag.x = e.clientX;
    drag.y = e.clientY;

}

export function RenderMouseUp(e) {

    dragging = false;

    pan.x += e.clientX - drag.x;
    pan.y += e.clientY - drag.y;

    drag.x = 0;
    drag.y = 0;

}

export function RenderMouseMove(e) {


    if(dragging) {

        let s = viewSize()
        ctx.clearRect(0, 0, s[0], s[1]);

        let dx = e.clientX - drag.x,
            dy = e.clientY - drag.y;
    
        let t = ctx.getTransform();
        t.e = pan.x + dx;
        t.f = pan.y + dy;
        ctx.setTransform(t);


        render();

    }

}

function render() {

    layout.lblocks.forEach(b => { renderBlock(ctx, b, data); })
    layout.fblocks.forEach(b => { renderBlock(ctx, b, data); })
    layout.sblocks.forEach(b => { renderBlock(ctx, b, data); })

}

function renderBlock(ctx, b, data) {

    ctx.lineWidth = 1;
    ctx.strokeStyle = blockStroke;
    ctx.setLineDash([2,2]);
    ctx.strokeRect(b.pos.x, b.pos.y, b.width, b.height);
    ctx.setLineDash([]);
    ctx.lineWidth = 0.5;

    ctx.fillStyle = switchFill;
    b.switches.forEach(s => {

        renderNode(ctx, b, s, switchFill);

    })

    ctx.fillStyle = nodeFill;
    b.nodes.forEach(n => {

        renderNode(ctx, b, n, nodeFill);

    })

}

function renderNode(ctx, b, n, fill) {

    let nx = b.pos.x + n.pos.x,
        ny = b.pos.y + n.pos.y;

    ctx.fillStyle = fill;
    ctx.fillRect(nx, ny, n.width, n.height);

    ctx.fillStyle = "#444";
    ctx.font = "8px monospace";
    let tx = nx + n.width/2 - ctx.measureText(n.name).width/2;
    ctx.fillText(n.name, tx, ny - nodeTextOffset);

    n.interfaces.forEach(ifx => {

        let ix = nx + ifx.pos.x,
            iy = ny + ifx.pos.y;

        ctx.font = "6px monospace";
        ctx.fillStyle = ifxFill;
        ctx.strokeStyle = ifxStroke;

        ctx.fillRect(ix, iy, ifxSize, ifxSize);
        ctx.strokeRect(ix, iy, ifxSize, ifxSize);

        ctx.fillStyle = "#444";
        let tx = ix + ifxSize/2 - ctx.measureText(ifx.neighbor.node).width/2,
            ty = iy + ifxSize/2;
        ctx.fillText(ifx.neighbor.node, tx, ty);

    })

}

function newLayout(ctx, data) {

    let x = blockSpacing,
        y = blockSpacing,
        _y = blockSpacing;

    // spine blocks

    let sblocks = []
    if(data.SpineBlocks != null) {

        sblocks = data.SpineBlocks.map(sb => {
            let b = newBlock(sb, data);
            b.pos.x = x;
            b.pos.y = y;
            x += b.width + blockSpacing;
            _y = Math.max(_y, b.height + tierSpacing);
            return b;
        });

        y += _y;

    }

    // fabric blocks
    
    let fblocks = [];
    if(data.FabricBlocks != null) {
    
        _y = blockSpacing,
        x = blockSpacing;
        //y += 200;

        fblocks = data.FabricBlocks.map(fb => {
            let b = newBlock(fb, data);
            b.pos.x = x;
            b.pos.y = y;
            x += b.width + blockSpacing;
            _y = Math.max(_y, b.height + tierSpacing);
            return b;
        });

        y += _y;

    }

    // leaf blocks
    
    let lblocks = [];
    if(data.LeafBlocks != null) {
    
        _y = blockSpacing,
        x = blockSpacing
        //y += 200

        lblocks = data.LeafBlocks.map(mb => {
            let b = newBlock(mb, data);
            b.pos.x = x;
            b.pos.y = y;
            x += b.width + blockSpacing;
            _y = Math.max(_y, b.height + tierSpacing);
            return b;
        });

        y += _y;

    }

    bounds.w = x
    bounds.h = y

    // result

    let result = {
        lblocks: lblocks,
        fblocks: fblocks,
        sblocks: sblocks
    };

    alignLayout(result);

    return result;

}

function alignLayout(layout) {

    for(var i=0; i<iters; i++) {

        layout.lblocks.forEach(lb => {
            lb.upstream.forEach(u => {

                let fb = layout.fblocks[u];
                if(fb == null) {
                    fb = layout.sblocks[u]
                }
                let pull = center(fb) - center(lb);
                fb.dx -= pull*step;

            })
        })

        for(var j=0; j<layout.fblocks.length; j++) {

            let f = layout.fblocks[j];

            if(f.dx > 0 && j<layout.fblocks.length-1) {

                let overlap = rightOverlap(f, layout.fblocks[j+1]);
                if(overlap > 0) {
                    f.dx = f.dx - overlap;
                    layout.fblocks[j+1].dx += overlap;
                }
            }

            if(f.dx < 0 && j > 0) {

                let overlap = leftOverlap(f, layout.fblocks[j-1]);
                if(overlap > 0) {
                    f.dx = f.dx + overlap
                    layout.fblocks[j-1].dx -= overlap;
                }

            }

        }

        layout.fblocks.forEach(fb => {
            fb.pos.x += fb.dx;
            fb.dx = 0;
        });

        layout.fblocks.forEach(fb => {
            fb.upstream.forEach(u => {

                let sb = layout.sblocks[u];
                let pull = center(sb) - center(fb);
                sb.dx -= pull*step;

            })
        })

        layout.sblocks.forEach(sb => {
            sb.pos.x += sb.dx;
            sb.dx = 0;
        });

        for(var j=0; j<layout.sblocks.length; j++) {

            let s = layout.sblocks[j];
            if(j<layout.sblocks.length-1) {

                let overlap = rightOverlap(s, layout.sblocks[j+1]);
                if(overlap > 0) {
                    s.dx = s.dx - overlap;
                    layout.sblocks[j+1].dx += overlap;
                }
            }

            if(j > 0) {

                let overlap = leftOverlap(s, layout.sblocks[j-1]);
                if(overlap > 0 && false) {
                    s.dx = s.dx + overlap
                    layout.sblocks[j-1].dx -= overlap;
                }

            }

        }

        layout.sblocks.forEach(sb => {
            sb.pos.x += sb.dx;
            sb.dx = 0;
        });

    }

    // push blocks away from each other until overlap no longer exists
    let again = false;
    do {

        again = false;

        for(var j=0; j<layout.sblocks.length; j++) {

            let s = layout.sblocks[j];
            if(j<layout.sblocks.length-1) {

                let overlap = rightOverlap(s, layout.sblocks[j+1]);
                if(overlap > 0) {
                    s.dx = s.dx - overlap;
                    layout.sblocks[j+1].dx += overlap;
                    again = true
                }
            }

            if(j > 0) {

                let overlap = leftOverlap(s, layout.sblocks[j-1]);
                if(overlap > 0) {
                    s.dx = s.dx + overlap
                    layout.sblocks[j-1].dx -= overlap;
                    again = true
                }

            }

        }

        layout.sblocks.forEach(sb => {
            sb.pos.x += sb.dx;
            sb.dx = 0;
        });

    } while(again)

}

function newBlock(mb, data) {

    let b = {
        switches: mb.Switches.map(x => newNode(
            x, data[data.TierMap[x]][x].Neighbors
        )),
        nodes: mb.Nodes.map(x => newNode(
            x, data[data.TierMap[x]][x].Neighbors
        )),
        width: 0,
        height: 0,
        pos: { x: 0, y: 0 },
        dx: 0,
        upstream: mb.Upstream,
        downstream: mb.Downstream,
    };

    // initial switch layout stacked horizontally from left to right

    let x = nodeSpacing;
    let maxY = 0;
    b.switches.forEach(s => {
        s.pos.x = x;
        s.pos.y = nodeSpacing;
        x += s.width + nodeSpacing;
        maxY = Math.max(maxY, nodeSpacing+s.height);
    });

    // initial node layout in a grid 
    
    let s = Math.floor(Math.sqrt(b.nodes.length));
    b.nodes.forEach((n, i) => {
        n.pos.x = nodeSpacing + Math.floor(i%s)*(n.width + nodeSpacing);
        n.pos.y = maxY + nodeSpacing + Math.floor(i/s)*(n.height + nodeSpacing);
    });

    let lastSwitch = b.switches[b.switches.length-1],
        rightNode = b.nodes[s-1],
        lastNode = b.nodes[b.nodes.length-1];

    let switchesWidth = lastSwitch.pos.x + lastSwitch.width + nodeSpacing;
    let nodesWidth = nodeSpacing;
    b.nodes.forEach(n => {
        nodesWidth = Math.max(
            nodesWidth,
            n.pos.x + n.width + nodeSpacing
        )
    })
    /*
    if(rightNode != null && lastNode != null){
        nodesWidth = rightNode.pos.x + rightNode.width + nodeSpacing;
    }
    */

    // center switches and nodes
    if(nodesWidth < switchesWidth) {
        b.nodes.forEach(n => {
            n.pos.x += (switchesWidth - nodesWidth)/2
        })
    }
    if(switchesWidth < nodesWidth) {
        b.switches.forEach(s => {
            s.pos.x += (nodesWidth - switchesWidth)/2
        })
    }

    b.width = Math.max(switchesWidth, nodesWidth);
    if(lastNode != null) {
        b.height = nodeSpacing + lastNode.pos.y + lastNode.height;
    }
    else {
        b.height = nodeSpacing + lastSwitch.pos.y + lastSwitch.height;
    }

    return b;

}

function newNode(name, neighbors) {

    let radix = neighbors.length;

    let n = {
        name: name,
        width: 0,
        height: 0,
        pos:  { x: 0, y: 0 },
        dx: 0,
        interfaces: [],
    };

    let s = Math.ceil(Math.sqrt(radix));
    n.width = ifxSpacing + s*(ifxSize + ifxSpacing);
    n.height = ifxSpacing + Math.ceil(radix/s)*(ifxSize + ifxSpacing);

    n.interfaces = neighbors.map((x, i) => {
        return {
            name: x.Local,
            neighbor: {
                node: x.Remote.Node,
                ifx: x.Remote.Interface,
            },
            pos: {
                x: ifxSpacing + Math.floor(i%s)*(ifxSize + ifxSpacing),
                y: ifxSpacing + Math.floor(i/s)*(ifxSize + ifxSpacing)
            }
        }
    });

    return n;

}

function center(obj) { return obj.pos.x + obj.width/2 }

function rightOverlap(a, b) {

    const aRightEdge = a.pos.x + a.width + nodeSpacing,
          bLeftEdge  = b.pos.x;

    return aRightEdge - bLeftEdge;

}

function leftOverlap(a, b) {

    const aLeftEdge  = a.pos.x,
          bRightEdge = b.pos.x + b.width + nodeSpacing;

    return bRightEdge - aLeftEdge;

}

function viewSize() {

    let w = ctx.canvas.clientWidth,
        h = ctx.canvas.clientHeight;

    let pt = worldCoords([w,h]);

    return pt

}

function screenCoords(p) {

    let t = ctx.getTransform();

    return [
        p[0]*t.a + p[1]*t.c + t.e,
        p[0]*t.b + p[1]*t.d + t.f,
    ]

}

function worldCoords(p) {

    let t = ctx.getTransform().invertSelf();

    return [
        p[0]*t.a + p[1]*t.c + t.e,
        p[0]*t.b + p[1]*t.d + t.f,
    ]

}

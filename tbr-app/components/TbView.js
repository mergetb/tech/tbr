import styles from './style.module.css';
import React, { useState, useEffect } from 'react';
import Render, { RenderZoom, RenderMouseDown, RenderMouseUp, RenderMouseMove } from '../lib/RenderV2'

export default function TbView(props) {

    const [width, setWidth] = useState(0)
    const [height, setHeight] = useState(0)
    useEffect(() => {

        setWidth(window.innerWidth)
        setHeight(window.innerHeight)

        const handleResize = () => {
            setWidth(window.innerWidth)
            setHeight(window.innerHeight)
            var cnvs = document.getElementById('notes');
            if(cnvs != null) {
              cnvs.width = window.innerWidth;
              cnvs.height = window.innerHeight;
            }
        }
        window.addEventListener('resize', handleResize)
        return() => { 
          window.removeEventListener('resize', handleResize) 
        }
    }, [])

    const [src, setSrc] = useState({});

    useEffect(() => {
        fetch(window.location.href+props.source)
            .then(response => response.json())
            .then(data => { setSrc(data); return data; })
            .then(data => Render(data));
    }, [props.source]);

    if(src == "") {
        return (
            <div className={styles.tbview}>
                <h3> Loading {props.source} ... </h3>
            </div>
        );
    }
    else {
        console.log(src);
        return (
            <div className={styles.tbview}>
                <canvas 
                    id="tbview" 
                    width={width} 
                    height={height}
                    onWheel={RenderZoom}
                    onMouseDown={RenderMouseDown}
                    onMouseUp={RenderMouseUp}
                    onMouseMove={RenderMouseMove}
                ></canvas>
            </div>
        )
    }

}


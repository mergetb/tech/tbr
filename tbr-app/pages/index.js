import Head from 'next/head';
import TbView from '../components/TbView';

export default function Home() {

  return (
    <div className="container">
      <Head>
        <title>TbRender</title>
        <link rel="icon" href="/merge.svg" />
      </Head>
      <div>
        <TbView source='out.json' />
      </div>
    </div>
  )

}


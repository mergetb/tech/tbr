package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	//lib "gitlab.com/mergetb/facilities/example/miniclos"
	//lib "gitlab.com/mergetb/facilities/example/spineleaf"
	//"gitlab.com/mergetb/facilities/dcomp/model"
	//lib "gitlab.com/praxistb/ops/praxis"
	lib "gitlab.com/mergetb/facilities/redstar/model"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type Node struct {
	Name       string
	Interfaces []string
	Neighbors  []Neighbor
}

type Link struct {
	Endpoints []Endpoint
}

type Neighbor struct {
	Local  string
	Remote Endpoint
}

type Endpoint struct {
	Node      string
	Interface string
}

type Diagram struct {
	Spine        map[string]Node
	Fabric       map[string]Node
	Leaf         map[string]Node
	Machine      map[string]Node
	Emulation    map[string]Node
	MachineOrder []string
	LeafBlocks   []*Block
	FabricBlocks []*Block
	SpineBlocks  []*Block
	TierMap      map[string]string
	Links        []Link
}

func NewDiagram() *Diagram {
	return &Diagram{
		Spine:     make(map[string]Node),
		Fabric:    make(map[string]Node),
		Leaf:      make(map[string]Node),
		Emulation: make(map[string]Node),
		Machine:   make(map[string]Node),
		TierMap:   make(map[string]string),
	}
}

func main() {

	log.SetFlags(0)
	testbed := lib.Topo()
	topo := tb.ToXir(testbed, "test")
	//topo := dcomp.DCompTB().Net()
	s, _ := topo.ToString()
	topo, _ = xir.FromString(s)

	dg := NewDiagram()

	machines := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.Node)
	})
	for _, x := range machines {
		dg.Machine[x.Label()] = DgNode(x)
		dg.TierMap[x.Label()] = "Machine"
		dg.MachineOrder = append(dg.MachineOrder, x.Label())
	}

	dg.LeafBlocks = leafBlocks(topo)
	dg.FabricBlocks = fabricBlocks(topo)
	dg.SpineBlocks = spineBlocks(topo)

	// lookup table for what switches in what blocks
	bm := make(map[string]int)
	for i, b := range dg.LeafBlocks {
		for _, s := range b.Switches {
			bm[s] = i
		}
	}
	for i, b := range dg.FabricBlocks {
		for _, s := range b.Switches {
			bm[s] = i
		}
	}
	for i, b := range dg.SpineBlocks {
		for _, s := range b.Switches {
			bm[s] = i
		}
	}

	spines := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Spine)
	})
	for _, x := range spines {
		dg.Spine[x.Label()] = DgNode(x)
		dg.TierMap[x.Label()] = "Spine"

		fabrics := selectNeighbors(x, tb.Fabric)
		for _, f := range fabrics {
			dg.SpineBlocks[bm[x.Label()]].AddDownstream(bm[f.Label()])
		}
	}

	fabrics := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Fabric)
	})
	for _, x := range fabrics {
		dg.Fabric[x.Label()] = DgNode(x)
		dg.TierMap[x.Label()] = "Fabric"

		spines := selectNeighbors(x, tb.Spine)
		for _, s := range spines {
			dg.FabricBlocks[bm[x.Label()]].AddUpstream(bm[s.Label()])
		}

		leaves := selectNeighbors(x, tb.Leaf)
		for _, l := range leaves {
			dg.FabricBlocks[bm[x.Label()]].AddDownstream(bm[l.Label()])
		}
	}

	leaves := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch) && tb.HasRole(x, tb.Leaf)
	})
	for _, x := range leaves {
		dg.Leaf[x.Label()] = DgNode(x)
		dg.TierMap[x.Label()] = "Leaf"

		fabrics := selectNeighbors(x, tb.Fabric, tb.Spine)
		for _, f := range fabrics {
			dg.LeafBlocks[bm[x.Label()]].AddUpstream(bm[f.Label()])
		}
	}

	emus := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.NetworkEmulator)
	})
	for _, x := range emus {
		dg.Emulation[x.Label()] = DgNode(x)
		dg.TierMap[x.Label()] = "Emulation"
	}

	for _, l := range topo.AllLinks() {
		add := false
		var link Link
		for _, e := range l.Endpoints {
			link.Endpoints = append(link.Endpoints, Endpoint{
				Node:      e.Endpoint.Parent.Label(),
				Interface: e.Endpoint.Label(),
			})
			if tb.HasRole(e.Endpoint.Parent, tb.XpSwitch) {
				add = true
			}
		}
		if add {
			dg.Links = append(dg.Links, link)
		}
	}

	out, err := json.MarshalIndent(dg, "", "  ")
	if err != nil {
		log.Fatal("failed to marshal JSON: %v", err)
	}

	err = ioutil.WriteFile("out.json", out, 0644)
	if err != nil {
		log.Fatal("failed to write JSON: %v", err)
	}

}

func DgNode(x *xir.Node) Node {

	n := Node{
		Name:      x.Label(),
		Neighbors: make([]Neighbor, 0),
	}

	r := tb.GetResourceSpec(x)

	isNode := r.HasRole(tb.Node)

	for _, e := range x.Endpoints {
		role := r.LinkRoles[e.Label()]
		if role == tb.XpLink || role == tb.EmuLink {
			n.Interfaces = append(n.Interfaces, e.Label())
			for _, nbr := range e.Neighbors {

				// handle breakout cables
				if isNode && tb.HasRole(nbr.Endpoint.Parent, tb.Node) {
					continue
				}

				n.Neighbors = append(n.Neighbors, Neighbor{
					Local: e.Label(),
					Remote: Endpoint{
						Node:      nbr.Endpoint.Parent.Label(),
						Interface: nbr.Endpoint.Label(),
					},
				})

			}
		}
	}

	return n

}
